# Split screen dance evaluation aid using machine learning

This project aims to provide a measurable metric (an average value of the difference in elbow angles of two dancers throughout a performance) so as to help judges score them more fairly. 

## Description



## Getting started

### Dependencies
- Jupyter Notebook 

Or any other platform that can run ipynb files. 

### Installation
_For LINUX users:_

- Install the classic Jupyter Notebook using
`pip install notebook` in the terminal.

- To run the notebook, enter `jupyter notebook` in the terminal.

For WINDOWS users:

- First, open a command prompt or terminal on your computer. For installation, pip should be upgraded using the command `python –m pip install –upgrade pip`.

- Now navigate to This PC > Local Disk(C) > Users > username > AppData > Local > Programs > Python > Python3.11 > Scripts. After going to the Scripts folder, follow the same steps as in the Linux section. 

### Executing the program

- Download the [video_input.ipynb](https://gitlab.com/xnash/split-screen-dance-evaluation-aid-using-machine-learning/-/blob/main/video_input.ipynb?ref_type=heads) file from this repository and upload it into your Jupyter Notebook. 

- Open it and run the cells. Make sure that the input name is changed to that of a dance video available in your system.

## Authors 

- [Nashita Noushad](https://gitlab.com/xnash)
- [Nirmal Michael](https://github.com/nirmalmichael11)
- [Nithin Raj](https://github.com/NithinRaj11)
- [Shifa Sageer](https://github.com/shifasageer)
